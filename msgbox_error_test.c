#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include "arch/x86/include/generated/uapi/asm/unistd_64.h"

int main(int argc, char ** argv) {
    const char testMessage[] = "wololo";
    char buffer[50];

    printf("%ld", strlen(testMessage));

    printf("\nTesting with message:");
    printf("\n%s", testMessage); //6 chars

    printf("\nmsgbox_put called with length -1: ");
    
    fflush(stdout);
    sleep(2.0f);
    
    errno = syscall(__NR_dm510_msgbox_put, testMessage, -1);
    printf("\nError : %s\n", strerror(abs(errno))); //expecting "Cannot assign request address"
    
    fflush(stdout);
    sleep(2.0f);
    
    printf("\nnow inputting correct length of message ");
    errno = syscall(__NR_dm510_msgbox_put, testMessage, sizeof(testMessage));
    printf("\nDid we succeed? : %s\n", strerror(abs(errno))); //expecting success

    printf("\nnow testing get with -1: ");
    
    fflush(stdout);
    sleep(2.0f);
    
    errno = syscall(__NR_dm510_msgbox_get, buffer, -1);
    printf("\nError: %s\n", strerror(abs(errno))); //expecting "Invalid argument"
    
    fflush(stdout);
    sleep(2.0f);

    printf("\nTrying to extract the message now using a correct size:");
    errno = syscall(__NR_dm510_msgbox_get, buffer, sizeof(testMessage));
    if(errno <= 0){
      printf("\n%s\n", strerror(abs(errno))); //expecting "Success"
    }
    printf("\nMessage gotten : \"%s\"\n", buffer);
    
    fflush(stdout);
    return 0;
}


