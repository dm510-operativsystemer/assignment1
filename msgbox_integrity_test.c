#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include "arch/x86/include/generated/uapi/asm/unistd_64.h"

int main(int argc, char** argv) {
    
    const char testMessage[] = "Hello, this is the correct string";
    char messageBuffer[50];
    int messageLength;

    printf("%ld", strlen(testMessage));
    
    printf("\nTesting with message:");
    printf("\n%s", testMessage); //33 chars

    printf("\nmsgbox_put called:");
    syscall(__NR_dm510_msgbox_put, testMessage, sizeof(testMessage));

    printf("\nmsgbox_get called:");
    messageLength = syscall(__NR_dm510_msgbox_get, messageBuffer, 50);    

    printf("\nThe message is still: \n");

    for(int i = 0; i < messageLength; i++){
      printf("%c", messageBuffer[i]);
    }
    printf("\n");

    printf("\nNow testing with more than one message:\n");
    char first[] = "first";
    char second[] = "second";
    char third[] = "third";

    printf("\nmsgbox_put called with message \"%s\":", first);
    syscall(__NR_dm510_msgbox_put, first, sizeof(first));
    printf("\nmsgbox_put called with message \"%s\":", second);
    syscall(__NR_dm510_msgbox_put, second, sizeof(second));
    printf("\nmsgbox_put called with message \"%s\":", third);
    syscall(__NR_dm510_msgbox_put, third, sizeof(third));

    printf("\nmsgbox_get called, expecting \"third\":\t");

    messageLength = syscall(__NR_dm510_msgbox_get, messageBuffer, 50);
    for(int i = 0; i < messageLength; i++){
      printf("%c", messageBuffer[i]);
    }
    printf("\nmsgbox_get called, expecting \"second\":\t");

    messageLength = syscall(__NR_dm510_msgbox_get, messageBuffer, 50);
    for(int i = 0; i < messageLength; i++){
      printf("%c", messageBuffer[i]);
    }

    printf("\nmsgbox_get called, expecting \"first\":\t");

    messageLength = syscall(__NR_dm510_msgbox_get, messageBuffer, 50);
    for(int i = 0; i < messageLength; i++){
      printf("%c", messageBuffer[i]);
    }
    printf("\n");

    return 0;
}