#ifndef __UML_DM510_MSGBOX_PUT_H__
#define __UML_DM510_MSGBOX_PUT_H__

#ifndef __UML_DM510_MSGBOX_GET_H__
#define __UML_DM510_MSGBOX_GET_H__

extern int sys_dm510_msgbox_put( char*, int )
extern int sys_dm510_msgbox_get( char*, int )

#endif 
