#include "linux/kernel.h"
#include "linux/unistd.h"
#include "linux/slab.h"
#include "linux/uaccess.h"

typedef struct _msg_t msg_t;

struct _msg_t{
  msg_t* previous;
  int length;
  char* message;
};

static msg_t *bottom = NULL;
static msg_t *top = NULL;

asmlinkage int sys_dm510_msgbox_put( char *buffer, int length ) {

  unsigned long flaggington;
  msg_t* msg; //declaration here to fix ISO C90 warning
  local_irq_save(flaggington); // saving the state of CPU interupt system (use for race condition reasons)

  msg = kmalloc(sizeof(msg_t), GFP_KERNEL);
  if(!msg){
    printk("kmalloc of msg failed");
    local_irq_restore(flaggington);
    return -EADDRNOTAVAIL;
  }
  msg->previous = NULL;
  msg->length = length;
  msg->message = kmalloc(length, GFP_KERNEL);
  if(!msg->message){
    printk("kmalloc of msg->message failed");
    local_irq_restore(flaggington);
    return -EADDRNOTAVAIL;
  }
  if(access_ok(msg->message, length) ){
    //calls copy_from_user and procedes to end part if succeed.
    if(copy_from_user(msg->message, buffer, length) != 0){
      printk("copy_from_user failed");
      local_irq_restore(flaggington);
      return -EINVAL;
    }
  }
  else{
    printk("access_ok put failed");
    local_irq_restore(flaggington);
    return -EFAULT;
  }
  if (bottom == NULL) {
    bottom = msg;
    top = msg;
  } else {
    /* not empty stack */
    msg->previous = top;
    top = msg;
  }
  local_irq_restore(flaggington);
  return 0;
}

asmlinkage int sys_dm510_msgbox_get( char* buffer, int length ) {

  unsigned long flaggington;
  local_irq_save(flaggington);

  if (top != NULL) {
    msg_t* msg = top;
    int mlength = msg->length;
    if(mlength <= length){
      top = msg->previous;
    } else {
      local_irq_restore(flaggington);
      return -EINVAL;
    }

    /* copy message */
    if(access_ok(buffer, length) != 0 ){
      if(copy_to_user(buffer, msg->message, msg->length) != 0){
        printk("copy_to_user failed");
        local_irq_restore(flaggington);
        return -EINVAL;
      }
    }
    else{
      printk("access_ok get failed");
      local_irq_restore(flaggington);
      return -EFAULT;
    }

    /* free memory */
    kfree(msg->message);
    kfree(msg);
    local_irq_restore(flaggington);
    return mlength;
  }
  local_irq_restore(flaggington);
  printk("msgbox_get failed");
  return -1;
}
